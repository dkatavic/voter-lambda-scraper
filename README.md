# Lambda Voter Scraper

Scraper that scraps information of all voters in specific year of the following url: http://sortedbybirthdate.com/

### How does it works

It's deployed as a single Lambda package that contains 3 functions:

* Scraper that scraps page with the calendar year and returns links of voter lists (#1)

* Scraper that scraps voter lists and returns links of voter detailed information (#2)

* Scraper that scraps voter detailed information and saves result to DB (#3)

This lambda app uses:

* AWS SQS for queuing and storing links of voter list and voter details information
* AWS RDS for storing voter details


\#1 Function scraps urls of the "voter lists" and stores each link as the message to AWS SQS. Then it invokes #2 Function.


\#2 Function pools SQS for "voter list" messages, and if it is found, it scraps "voter details information" urls, stores each link as a message to AWS SQS and invokes #2 again. If there are no messages, #2 invokes X number of #3 functions, where X is concurrency level.


\#3 Function pools SQS for "voter details information" messages, and if is found, it scraps for voter info, stores it in RDS and invokes #3. If there are no messages, AWS Lambda exits


### Requirements


* 4 AWS SQS
* AWS RDS
* IAM role with required permissions

There needs to be at least 2 AWS SQS, 1 for storing "voter lists" and other for storing "voter details information". I would recommend 4 AWS SQS, so that each additional SQS serves as dead letter queue to the first two. Example of configuration of AWS SQS for "voter list" https://www.dropbox.com/s/n7naex8gcizgnwh/Screenshot%202016-06-24%2011.55.02.png?dl=0

Set up IAM role with the following permissions:

* AWSLambdaFullAccess For invoking other lambdas
* AmazonRDSFullAccess
* AmazonSQSFullAccess

### Configuring

All configurations are stored in a config.js file. You need to provide:

* lambdaFunctionName: This is the name of the lambda function you are going to set. It's required for recursive calling of lambdas
* scrapYear: Year to scrap
* rds: RDS data
* sqs: SQS data 

Handler: index.handle

### Deploying

You can deploy using:

* apex framework and provided git repo with apex deploy scrap
* manually with provided .zip file. This is the easiest solution

### Starting

You can start just by invoking lambda manually or with apex. If you want to invoke #2 or #3 lambda:

* For invoking #2 lambda, launch lambda with event data {type: 'scrapVoterList'}
* For invoking #3 lambda, launch lambda with event data {type: 'scrapVoterInfo'}

### Stoping

If you want to stop Lambda, you can just purge AWS SQS queue. If for some reason that doesn't work (but it should), you can remove AWSLambdaFullAccess Policy from provided IAM role to be sure