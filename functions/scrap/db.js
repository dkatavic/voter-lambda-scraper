// db.js
var Sequelize = require('sequelize'),
    config = require('./config');

var sequelize, Voter;

module.exports = {

  authenticate: function() {

    sequelize = new Sequelize(config.rds.dbName, config.rds.user, config.rds.password, {
      host: config.rds.host,
      dialect: 'mysql',
      pool: {
        max: 3,
        min: 1,
        idle: 10000
      },
      logging: false
    });

    Voter = sequelize.define('voter', {
      firstName: {
        type: Sequelize.STRING
      },
      middleName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      birthdate: {
        type: Sequelize.STRING
      },
      addressCity: {
        type: Sequelize.STRING
      },
      addressZip: {
        type: Sequelize.INTEGER
      },
      addressStreet: {
        type: Sequelize.STRING
      },
      registrationDate: {
        type: Sequelize.STRING
      },
      phoneArea: {
        type: Sequelize.INTEGER
      },
      phoneNumber: {
        type: Sequelize.INTEGER
      },
      email: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.STRING
      },
      race: {
        type: Sequelize.STRING
      }

    }, {}); 

    return sequelize.authenticate()
    .catch(function (err) {
      console.log('Unable to connect to the database:', err);
      return err;
    });

  },

  /**
  * @method saveVoterData
  * @description save voter data to RDS
  *
  * @param {Object} params voter info
  */

  saveVoterData: function(params) {

    return Voter.create({
      firstName: params.firstName,
      middleName: params.middleName,
      lastName: params.lastName,
      birthdate: params.birthdate,
      addressCity: params.address.city,
      addressZip: params.address.zip,
      addressStreet: params.address.street,
      registrationDate: params.registration,
      phoneArea: params.phone.area,
      phoneNumber: params.phone.number,
      email: params.email,
      gender: params.gender,
      race: params.race
    });

  },

  /**
  * Sync tables from ORM to DB
  */

  sync: function(params) {
    return sequelize.sync();

  },

  close: function(){
    sequelize.close();
    return Promise.resolve(true);
  }

}