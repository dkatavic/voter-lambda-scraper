var AWS = require('aws-sdk'),
    _ = require('lodash'),
    url = require('url'),
    uuid = require('uuid'),
    config = require('./config');

var sqs = new AWS.SQS();

var personInfoQueueUrl = config.sqs.personInfoQueueUrl;
var personListQueueUrl = config.sqs.personListQueueUrl;

module.exports = {

  /**
  * @method getPersonInfoMessage
  * @description get person info message from queue
  *
  * @return {Object} with data and ReceiptHandle property if exists, null if not
  */

  getPersonInfoMessage: function(){

    var opts = {
      QueueUrl: personInfoQueueUrl,
      AttributeNames: [],
      MaxNumberOfMessages: 1,
      MessageAttributeNames: [],
      VisibilityTimeout: 10,
      WaitTimeSeconds: 0
    };

    return new Promise((resolve, reject) => {

      sqs.receiveMessage(opts, function(err, data) {

        if (err) {
          return reject(err);
        }

        if (!data.Messages || !data.Messages.length) {
          return resolve(null);
        }

        var messBody = data.Messages[0].Body;

        var output = {
          ReceiptHandle: data.Messages[0].ReceiptHandle,
          data: JSON.parse(messBody)
        };

        resolve(output);

      });

    });

  },

  /**
  * @method getPersonListMessage
  * @description get person list message from queue
  *
  * @return {Object} with data and ReceiptHandle property if exists, null if not
  */

  getPersonListMessage: function(){

    var opts = {
      QueueUrl: personListQueueUrl,
      AttributeNames: [],
      MaxNumberOfMessages: 1,
      MessageAttributeNames: [],
      VisibilityTimeout: 10,
      WaitTimeSeconds: 0
    };

    return new Promise((resolve, reject) => {

      sqs.receiveMessage(opts, function(err, data) {

        if (err) {
          return reject(err);
        }

        if (!data.Messages || !data.Messages.length) {
          return resolve(null);
        }

        var messBody = data.Messages[0].Body;

        var output = {
          ReceiptHandle: data.Messages[0].ReceiptHandle,
          data: JSON.parse(messBody)
        };

        resolve(output);

      });

    });

  },

  /**
  * @method deletePersonInfoMessage
  *
  * @param {String} ReceiptHandle handle of the queue message
  * @return {Boolean}
  */

  deletePersonInfoMessage: function(params){

    if (!params.ReceiptHandle) {
      throw new Error("Missing ReceiptHandle property");
    }

    var params = {
      QueueUrl: personInfoQueueUrl,
      ReceiptHandle: params.ReceiptHandle
    };

    return new Promise((resolve, reject) => {

      sqs.deleteMessage(params, function(err, data) {

        if (err) {
          return reject(err);
        }
        resolve(data);

      });

    });

  },

  /**
  * @method deletePersonListMessage
  *
  * @param {String} ReceiptHandle handle of the queue message
  * @return {Boolean}
  */

  deletePersonListMessage: function(params){

    if (!params.ReceiptHandle) {
      throw new Error("Missing ReceiptHandle property");
    }

    var params = {
      QueueUrl: personListQueueUrl,
      ReceiptHandle: params.ReceiptHandle
    };

    return new Promise((resolve, reject) => {

      sqs.deleteMessage(params, function(err, data) {

        if (err) {
          return reject(err);
        }
        resolve(data);

      });

    });

  },

  /**
  * @method sendPersonInfoMessageBatch
  * @description Send batch messages containing person info data
  *
  * @param {Array} urls Array of urls pointing to person info data
  * @return {Boolean}
  */

  sendPersonInfoMessageBatch: function(params) {

    if (!params.urls) {
      throw new Error("Missing urls property");
    }

    if (!Array.isArray(params.urls)) {
      throw new Error("urls needs to be a Array");
    }

    // sort uniques

    params.urls = _.uniq(params.urls);

    // filter unparsable urls

    params.urls = params.urls.filter(personInfoUrl => {
      var host = url.parse(personInfoUrl).host;
      return config.parsableHosts.indexOf(host) !== -1;
    });

    if (params.urls.length == 0) {
      return Promise.resolve(true);
    }

    var entries = params.urls.map(url => {
      return {
        Id: uuid.v1(),
        MessageBody: JSON.stringify({
          url: url
        })
      }
    });
    // maximum number of entries per request is limited to 10
    var entriesChunks = _.chunk(entries, 10);
    // array of batchs, each element in the array is sendMessageBatch Promise
    var arrayOfBatchs = entriesChunks.map(entries => {

      var opts = {
        Entries: entries,
        QueueUrl: personInfoQueueUrl
      };

      return new Promise((resolve, reject) => {

        sqs.sendMessageBatch(opts, function(err, data) {
          if (err) {
            return reject(err);
          }
          // data isn't required
          resolve(true);
        });

      });

    });


    return Promise.all(arrayOfBatchs);

  },

  /**
  * @method sendPersonInfoMessageBatch
  * @description Send batch messages containing person info data
  *
  * @param {Array} urls Array of urls pointing to person info data
  * @return {Boolean}
  */

  sendPersonListMessageBatch: function(params) {

    if (!params.urls) {
      throw new Error("Missing urls property");
    }

    if (!Array.isArray(params.urls)) {
      throw new Error("urls needs to be a Array");
    }

    if (params.urls.length == 0) {
      return Promise.reject(new Error("There are no parsed links from calendar"));
    }

    var entries = params.urls.map(url => {
      return {
        Id: uuid.v1(),
        MessageBody: JSON.stringify({
          url: url
        })
      }
    });
    // maximum number of entries per request is limited to 10
    var entriesChunks = _.chunk(entries, 10);
    // array of batchs, each element in the array is sendMessageBatch Promise
    var arrayOfBatchs = entriesChunks.map(entries => {

      var opts = {
        Entries: entries,
        QueueUrl: personListQueueUrl
      };

      return new Promise((resolve, reject) => {

        sqs.sendMessageBatch(opts, function(err, data) {
          if (err) {
            return reject(err);
          }
          // data isn't required
          resolve(true);
        });

      });

    });


    return Promise.all(arrayOfBatchs);

  }

};