module.exports = {

  // hosts that provide parsable person info. Don't change this
  parsableHosts: ['ohiovoters.info', 'oklavoters.com', 'flvoters.com', 'connvoters.com'],
  // REQUIRED! you must name the function with the same name as in AWS Lambda Console
  lambdaFunctionName: 'voter-scrap',
  // voter info concurrency
  voterInfoConcurrency: 35,
  // Year to scrap
  scrapYear: 1981,
  // RDS data
  rds: {
    host: "upwork.cu92xdbvfjij.eu-west-1.rds.amazonaws.com",
    user: "root",
    password: "PASSWORD",
    dbName: "voter_scraper"
  },
  // AWS SQS data
  sqs: {
    personInfoQueueUrl: 'https://sqs.eu-west-1.amazonaws.com/797895061669/upwork-scrap-voters',
    personListQueueUrl: 'https://sqs.eu-west-1.amazonaws.com/797895061669/upwork-scrap-list'
  }

};