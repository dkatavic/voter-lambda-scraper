var parsers = require('./parsers'),
    scrapers = require('./scrapers'),
	  config = require('./config'),
    _ = require("lodash"),
    lambda = require('./lambda');

// set global
global.config = config;

module.exports = {

  /**
  * @method handle
  * @description entry point into app
  *
  * @param {Object} event AWS Lambda event
  * @param {String} event.type ('scrapVoterInfo'|'scrapVoterList'|'start')
  *
  */

  handle: function(event, context, cb){

    console.log("Invoked scraper");
    console.log(event);

    var delegate;

    if (event.type === 'start' || event.type == null) {

      var db = require('./db');

      delegate = scrapers.calendar()
      .then(() => {
        return db.authenticate();
      })
      .then(() => {
        // Ensure that DB is synced. Call only on start because of performance
        return db.sync();
      })
      .then(() => {
        db.close();
        return lambda.invoke({
          event: {
            type: 'scrapVoterList'
          }
        });
      })
    }
    else if (event.type === 'scrapVoterList') {

      delegate = scrapers.voterlist()
      .then(data => {
        if (data.queueEmpty) {
          // invoke #3 lambda to parse voter ingo
          console.log("VoterList Queue Empty");
          var lambdas = _.times(config.voterInfoConcurrency, lambda.invoke({
            event: {
              type: 'scrapVoterInfo'
            }
          }));

          return Promise.all(lambdas);
        }
        else {
          // invoke next lambda of the same type
          return lambda.invoke({
            event: {
              type: 'scrapVoterList'
            }
          });
        }
      });

    }
    else if (event.type === 'scrapVoterInfo') {

      delegate = scrapers.voterinfo()
      .then(data => {
        if (data.queueEmpty) {
          console.log("VoterInfo Queue Empty");
          return data.message;
        }
        else {
          // invoke next lambda
          return lambda.invoke({
            event: {
              type: 'scrapVoterInfo'
            }
          });
        }
      })
      
    }
    else {
      throw new Error("Unsuported event type. Allowed values 'scrapVoterInfo'|'scrapVoterList'|'start'"); 
    }

    delegate.then(data => {
      cb(null, data);
      setTimeout(function(){
        // enforce process exit
        process.exit(0);
      }, 3000);
    })
    .catch(err => cb(err));

  }
}