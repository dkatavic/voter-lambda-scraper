// index.js
var cheerio = require('cheerio'),
    gender = require('gender');

/**
* method getFirstName
* @param {Object} $ jQuery of info table
* @return {String} first name
*/

function getFirstName($) {

  var name = $.find('tr:nth-child(5) td:nth-child(2)').text();
  name = name.trim();
  return name;

}

/**
* method getLasttName
* @param {Object} $ jQuery of info table
* @return {String} last name
*/

function getLasttName($) {

  var name = $.find('tr:nth-child(4) td:nth-child(2)').text();
  name = name.trim();
  return name;

}

/**
* method getMiddleName
* @param {Object} $ jQuery of info table
* @return {String} middle name
*/

function getMiddleName($) {

  var name = $.find('tr:nth-child(6) td:nth-child(2)').text();
  name = name.trim();
  return name;

}

/**
* method getBirthdate
* @param {Object} $ jQuery of info table
* @return {String} birthdate in MM/DD/YYYY format
*/

function getBirthdate($) {

  var birthdate = $.find('tr:nth-child(8) td:nth-child(2)').text();
  birthdate = birthdate.trim();
  return birthdate;

}

/**
* method getAdress
* @param {Object} $ jQuery of info table
* @return {String} address
*/

function getAdress($) {

  var address = $.find('tr:nth-child(11) td:nth-child(2)').text();
  address = address.trim();
  address = address.replace(/  */g, " ");

  return address;

}

/**
* method getZipCode
* @param {Object} $ jQuery of info table
* @return {String} zip code
*/

function getZipCode($) {

  var zipCode = $.find('tr:nth-child(15) td:nth-child(2)').text();
  zipCode = zipCode.trim();

  return zipCode;

}

/**
* method getCity
* @param {Object} $ jQuery of info table
* @return {String} city
*/

function getCity($) {

  var city = $.find('tr:nth-child(13) td:nth-child(2)').text();
  city = city.trim();
  city = city.replace(/  */g, " ");

  return city;

}

/**
* method getRegDate
* @param {Object} $ jQuery of info table
* @return {String} reg date MM/DD/YYYY
*/

function getRegDate($) {

  var regDate = $.find('tr:nth-child(9) td:nth-child(2)').text();
  regDate = regDate.trim();

  return regDate;

}

/**
* method getGender
* @param {String} name firstName + Lastname
* @return {String} gender
*/

function getGender(name) {

  var guess = gender.guess(name);

  if (guess.confidence && guess.confidence > 0.95) {
    return guess.gender;
  }
  else {
    return null;
  }

}

module.exports = {

  /**
  *  @method scrap
  *  @description scrap person informations from flvoters page source 
  *
  *  @param {String} data Webpage source from floverrs.com
  *  @return {Object} person object (see person.json)
  */

  scrap: function(data) {

    // load HTML to cheerio
    var $ = cheerio.load(data);
    // get last info table
    var infoTable = $("table table").last();

    var firstName = getFirstName(infoTable);
    var lastName = getLasttName(infoTable);

    var person = {
      "firstName": firstName,
      "middleName": getMiddleName(infoTable),
      "lastName": lastName,
      "birthdate": getBirthdate(infoTable),
      "address": {
        "city": getCity(infoTable),
        "zip": getZipCode(infoTable),
        "street": getAdress(infoTable)
      },
      "registration": getRegDate(infoTable),
      // there is no phone informations for ohio voters
      "phone": {
      },
      // there is no email informations for ohio voters
      "email": undefined,
      "gender": getGender(firstName + " " + lastName),
      // there is no race informations
      "race": undefined
    };

    return person;

  }
}