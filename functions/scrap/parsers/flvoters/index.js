// index.js
var cheerio = require('cheerio'),
    jsdom = require("jsdom").jsdom;

/**
* method getFirstName
* @param {Object} $ jQuery of info table
* @return {String} first name
*/

function getFirstName($) {

  var name = $.find('tr:nth-child(6)').find("nobr").text();
  name = name.trim();
  return name;

}

/**
* method getLasttName
* @param {Object} $ jQuery of info table
* @return {String} last name
*/

function getLasttName($) {

  var name = $.find('tr:nth-child(4)').find("nobr").text();
  name = name.trim();
  return name;

}

/**
* method getMiddleName
* @param {Object} $ jQuery of info table
* @return {String} middle name
*/

function getMiddleName($) {

  var name = $.find('tr:nth-child(7)').find("nobr").text();
  name = name.trim();
  return name;

}

/**
* method getBirthdate
* @param {Object} $ jQuery of info table
* @return {String} birthdate in MM/DD/YYYY format
*/

function getBirthdate($) {

  var birthdate = $.find('tr:nth-child(23)').find("nobr").text();
  birthdate = birthdate.trim();
  return birthdate;

}

/**
* method getAdress
* @param {Object} $ jQuery of info table
* @return {String} address
*/

function getAdress($) {

  var address = $.find('tr:nth-child(9)').find("nobr").text();
  address = address.trim();
  address = address.replace(/  */g, " ");

  return address;

}

/**
* method getZipCode
* @param {Object} $ jQuery of info table
* @return {String} zip code
*/

function getZipCode($) {

  var zipCode = $.find('tr:nth-child(13)').find("nobr").text();
  zipCode = zipCode.trim();

  return zipCode;

}

/**
* method getCity
* @param {Object} $ jQuery of info table
* @return {String} city
*/

function getCity($) {

  var city = $.find('tr:nth-child(11)').find("nobr").text();
  city = city.trim();
  city = city.replace(/  */g, " ");

  return city;

}

/**
* method getRegDate
* @param {Object} $ jQuery of info table
* @return {String} reg date MM/DD/YYYY
*/

function getRegDate($) {

  var regDate = $.find('tr:nth-child(24)').find("nobr").text();
  regDate = regDate.trim();

  return regDate;

}

/**
* method getPhoneNumber
* @param {Object} $ jQuery of info table
* @return {String} phone number
*/

function getPhoneNumber($) {

  var phoneNumber = $.find('tr:nth-child(37)').find("nobr").text();
  phoneNumber = phoneNumber.trim();

  return phoneNumber;

}

/**
* method getPhoneArea
* @param {Object} $ jQuery of info table
* @return {String} phone area code
*/

function getPhoneArea($) {

  var areaCode = $.find('tr:nth-child(36)').find("nobr").text();
  areaCode = areaCode.trim();

  return areaCode;

}

/**
* method getEmail
* @param {Object} $ jQuery of info table
* @return {String} email
*/

function getEmail($) {

  var email = $.find('tr:nth-child(38)').find("nobr").text();
  email = email.trim();

  return email;

}

/**
* method getGender
* @param {Object} $ jQuery of info table
* @return {String} gender
*/

function getGender($) {

  var gender = $.find('tr:nth-child(21)').find("nobr").text();
  gender = gender.trim().toLowerCase();

  if (gender == "m")
    gender = "male";

  else if (gender == "f")
    gender = "female";

  else
    gender = "";

  return gender;

}

/**
* method getRace
* @param {Object} $ jQuery of webpage
* @return {String} email
*/

function getRace($) {

  var race;
  var description = $('table big font').first().text();
  description = description.trim();

  //remove double whitespace
  description = description.replace(/  */g, " ");
  descriptionMatch = description.match(/e is listed as (.*)\./);

  if (descriptionMatch && descriptionMatch.length && descriptionMatch[1])
    race = descriptionMatch[1];
  else
    race = null;

  return race;

}


module.exports = {

	/**
	*  @method scrap
  *  @description scrap person informations from flvoters page source 
  *
  *  @param {String} data Webpage source from floverrs.com
  *  @return {Object} person object (see person.json)
	*/

	scrap: function(data) {

    // HTML data is badly formated, I'll need to parse it throuth jsparse to avoid chrome automatic fixes
    var jsdom = require("jsdom").jsdom;
    var document = jsdom(data);
    var window = document.defaultView;
 
    data = window.document.documentElement.outerHTML;
    // load HTML to cheerio
    var $ = cheerio.load(data);
    // get last info table
    var infoTable = $("table table center b").last().parent().parent();


    var person = {
      "firstName": getFirstName(infoTable),
      "middleName": getMiddleName(infoTable),
      "lastName": getLasttName(infoTable),
      "birthdate": getBirthdate(infoTable),
      "address": {
        "city": getCity(infoTable),
        "zip": getZipCode(infoTable),
        "street": getAdress(infoTable)
      },
      "registration": getRegDate(infoTable),
      "phone": {
        "area": getPhoneArea(infoTable),
        "number": getPhoneNumber(infoTable)
      },
      "email": getEmail(infoTable),
      "gender": getGender(infoTable),
      "race": getRace($)
    };

    return person;

	}
}