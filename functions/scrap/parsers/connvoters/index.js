// index.js
var cheerio = require('cheerio');

/**
* method getFirstName
* @param {Object} $ jQuery of info table
* @return {String} first name
*/

function getFirstName($) {

  var name = $.find("tr:nth-child(5) td:nth-child(2)").text();
  name = name.trim();
  return name;

}

/**
* method getLasttName
* @param {Object} $ jQuery of info table
* @return {String} last name
*/

function getLasttName($) {

  var name = $.find("tr:nth-child(4) td:nth-child(2)").text();
  name = name.trim();
  return name;

}

/**
* method getMiddleName
* @param {Object} $ jQuery of info table
* @return {String} middle name
*/

function getMiddleName($) {

  var name = $.find("tr:nth-child(6) td:nth-child(2)").text();
  name = name.trim();
  return name;

}

/**
* method getBirthdate
* @param {Object} $ jQuery of info table
* @return {String} birthdate in MM/DD/YYYY format
*/

function getBirthdate($) {

  var birthdate = $.find("tr:nth-child(39) td:nth-child(2)").text();
  birthdate = birthdate.trim();
  return birthdate;

}

/**
* method getAdress
* @param {Object} $ jQuery of info table
* @return {String} address
*/

function getAdress($) {

  var address = $.find("tr:nth-child(32) td:nth-child(2)").text();
  address = address.trim();
  address = address.replace(/  */g, " ");

  var addressNumber = $.find("tr:nth-child(30) td:nth-child(2)").text();
  addressNumber = addressNumber.trim();
  addressNumber = addressNumber.replace(/  */g, " ");

  return address + " " + addressNumber;

}

/**
* method getZipCode
* @param {Object} $ jQuery of info table
* @return {String} zip code
*/

function getZipCode($) {

  var zipCode = $.find("tr:nth-child(27) td:nth-child(2)").text();
  zipCode = zipCode.trim();

  return zipCode;

}

/**
* method getCity
* @param {Object} $ jQuery of info table
* @return {String} city
*/

function getCity($) {

  var city = $.find('tr:nth-child(11)').find("nobr").text();
  city = city.trim();
  city = city.replace(/  */g, " ");

  return city;

}

/**
* method getRegDate
* @param {Object} $ jQuery of info table
* @return {String} reg date MM/DD/YYYY
*/

function getRegDate($) {

  var regDate = $.find("tr:nth-child(44) td:nth-child(2)").text();
  regDate = regDate.trim();

  return regDate;

}

/**
* method getPhoneNumber
* @param {Object} $ jQuery of info table
* @return {String} phone number
*/

function getPhoneNumber($) {

  var phoneNumber = $.find("tr:nth-child(40) td:nth-child(2)").text();
  phoneNumber = phoneNumber.trim().substr(3);

  return phoneNumber;

}

/**
* method getPhoneArea
* @param {Object} $ jQuery of info table
* @return {String} phone area code
*/

function getPhoneArea($) {

  var areaCode = $.find("tr:nth-child(40) td:nth-child(2)").text();
  areaCode = areaCode.trim().substr(0,3);

  return areaCode;

}

/**
* method getGender
* @param {Object} $ jQuery of info table
* @return {String} gender
*/

function getGender($) {

  var gender = $.find("tr:nth-child(43) td:nth-child(2)").text();
  gender = gender.trim().toLowerCase();

  if (gender == "m")
    gender = "male";

  else if (gender == "f")
    gender = "female";

  else
    gender = "";

  return gender;

}


module.exports = {

  /**
  *  @method scrap
  *  @description scrap person informations from flvoters page source 
  *
  *  @param {String} data Webpage source from floverrs.com
  *  @return {Object} person object (see person.json)
  */

  scrap: function(data) {

    // load HTML to cheerio
    var $ = cheerio.load(data);
    // get last info table
    var infoTable = $("table").eq(3).find("table");

    var person = {
      "firstName": getFirstName(infoTable),
      "middleName": getMiddleName(infoTable),
      "lastName": getLasttName(infoTable),
      "birthdate": getBirthdate(infoTable),
      "address": {
        "city": getCity(infoTable),
        "zip": getZipCode(infoTable),
        "street": getAdress(infoTable)
      },
      "registration": getRegDate(infoTable),
      "phone": {
        "area": getPhoneArea(infoTable),
        "number": getPhoneNumber(infoTable)
      },
      // there is no email data for conneticum voters
      "email": undefined,
      "gender": getGender(infoTable),
      // there is no race info
      "race": undefined
    };

    return person;

  }
}