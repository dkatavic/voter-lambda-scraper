var connvoters = require('./connvoters'),
    flvoters = require('./flvoters'),
    ohiovoters = require('./ohiovoters'),
    oklavoters = require('./oklavoters'),
    sortedlist = require('./sortedlist');

module.exports = {

  /**
  * @description parse page with user informations (click here for more inforamtions link)
  */

  parse: function(params) {

    var person;

    switch (params.host) {
      case 'ohiovoters.info':
        person = ohiovoters.scrap(params.body);
        break;
      case 'connvoters.com':
        person = connvoters.scrap(params.body);
        break;
      case 'flvoters.com':
        person = flvoters.scrap(params.body);
        break;
      case 'oklavoters.com':
        person = oklavoters.scrap(params.body);
        break;
      default:
        throw new Error("Unknown host: ", params.host);
    }

    return person;

  },

  /**
  * @description parse page with list of users
  */

  parseList: function(params) {

    return sortedlist(params.body);

  }

};