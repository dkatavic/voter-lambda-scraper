// sortedlist.js

var cheerio = require('cheerio'),
    jsdom = require("jsdom").jsdom;

/**
* @description parse raw HTML and get links of all voters
*
* @param {String} data raw HTML of page, e.g. view-source:http://sortedbybirthdate.com/pages/19800818.html
*/

module.exports = function(data) {

  var links = data.match(/.*free/g);
  
  links = links.map(link => {
    try {
      return link.match(/href="(.*)">/)[1];
    }
    catch(e) {
      return null;
    }
  });

  links = links.filter(link => !!link);

  return links;

};
