// lambda.js

var AWS = require('aws-sdk'),
    config = require('./config');

var lambda = new AWS.Lambda({apiVersion: '2015-03-31'});

module.exports = {

  /**
  * @method invoke
  * @description Invoke Lambda function
  *
  * @param {String} event Event for newly invoked lambda function
  */

  invoke: function(params) {

    var opts = {
      FunctionName: config.lambdaFunctionName,
      InvocationType: 'Event',
      LogType: 'None',
      Payload: JSON.stringify(params.event)
    };

    return lambda.invoke(opts).promise();

  }

};
