// voterinfo.js

var request = require('request-promise'),
    parsers = require('../parsers'),
    queue = require('../queue'),
    db = require('../db'),
    url = require('url');
    
/**
* @description get userinfo job from queue, scrap it and save result to RDS
*/

module.exports = function(params) {

  // 1.) get job
  // 2.) create request
  // 3.) parse it
  // 4.) save it to DB
  // 5.) delete message

  var message, person;

  return queue.getPersonInfoMessage()
  .then(_message => {

    if (!_message) {
      return {
        message: "SQS users queue empty",
        queueEmpty: true
      };
    }

    message = _message;

    var scrapUrl = message.data.url;
    var host = url.parse(scrapUrl).host;
    // check is host parsable or is it some dummy link without person data (for dead ones and some special cases)
    if (config.parsableHosts.indexOf(host) == -1) {
      return {
        message: "Url isn't parsable"
      };
    }
    console.log("parsing url: ", scrapUrl);

    return request(scrapUrl)
    .then((body) => {
      person = parsers.parse({
        host: host,
        body: body
      });
      
      if (!person.firstName) {
        // something is wrong. log it and skip this person
        console.error("Can't parse firstname, url: " + scrapUrl);
        return Promise.resolve({
          skip: true
        })
      }

      return db.authenticate();
    })
    .then(() => {

      return db.saveVoterData(person);
    })
    .then(person => {

      db.close();

      if (person.skip) {
        return Promise.resolve(person);
      }

      var params = {
        ReceiptHandle: message.ReceiptHandle
      }

      return queue.deletePersonInfoMessage(params);
    });

  });


};
