// voterlist.js

var request = require('request-promise'),
    parsers = require('../parsers'),
    queue = require('../queue'),
    url = require('url');

module.exports = function(params) {

  // 1.) Get Message
  // 1.1) If there are no messages, invoke #3 Lambda
  // 2.) Scrap page
  // 3.) Push voterInfo messages
  // 4.) Delete Voter List message
  // 5.) Invoke #2 lambda

  var message;

  return queue.getPersonListMessage()
  .then(_message => {
    message = _message;

    if (!message) {
      return {
        message: "SQS voterlist queue empty",
        queueEmpty: true
      };
    }

    var scrapUrl = message.data.url;

    return request(scrapUrl)
    .then(body => {

      var urls = parsers.parseList({
        body: body
      });

      return queue.sendPersonInfoMessageBatch({
        urls: urls
      });

    })
    .then(batchs => {

      var params = {
        ReceiptHandle: message.ReceiptHandle
      }

      return queue.deletePersonListMessage(params);

    });

  });

};
