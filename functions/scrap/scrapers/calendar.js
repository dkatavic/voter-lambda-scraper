// calendar.js

// moment is more reliable then JS Date Class
var moment = require("moment"),
    queue = require('../queue');

/**
* @description generate all links in calendar section http://sortedbybirthdate.com/calendars/1980.html
*       As we know that all dates are gonna have link and list link by date is predictable, we can use this fact
*       to speed up generating links without visiting site mentioned above
*/

module.exports = function(params) {

  // TODO: set this in config
  var year = config.scrapYear;  

  var date = moment(new Date(year, 0, 1));
  // Link example http://sortedbybirthdate.com/pages/19800818.html
  var urls = [],
      linkBase = 'http://sortedbybirthdate.com/pages/',
      linkExt = '.html';

  while (date.year() != year + 1) {
    // ensure there is leading zero
    var month = ("0" + (date.month() + 1)).substr(-2);
    var day = ("0" + (date.date() + 1)).substr(-2);

    var link = linkBase + date.year() + month + day + linkExt;
    urls.push(link);
    date.date(date.date() + 1);
  }

  return queue.sendPersonListMessageBatch({
    urls: urls
  });

};
