// index.js

module.exports = {

  voterinfo: require('./voterinfo'),
  voterlist: require('./voterlist'),
  calendar: require('./calendar')
    
};
